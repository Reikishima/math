import 'package:flutter/material.dart';
import 'package:health/utils/app_colors.dart';
import 'package:health/widget/container1.dart';
import 'package:health/widget/container2.dart';
import 'package:health/widget/container3.dart';
import 'package:health/widget/container4.dart';
import 'package:health/widget/container5.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../utils/size_config.dart';

class Message extends StatelessWidget {
  const Message({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.whiteBackground,
        shadowColor: Colors.transparent,
        leading: Padding(
          padding: EdgeInsets.only(left: SizeConfig.horizontal(3)),
          child: const CircleAvatar(
            backgroundImage: AssetImage('assets/images/download.jpg'),
          ),
        ),
        title: Text(
          'Student',
          style: TextStyle(color: AppColors.darkBackground),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(
              Icons.more_vert,
              color: AppColors.darkBackground,
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          width: SizeConfig.screenWidth,
          height: SizeConfig.screenHeight,
          color: AppColors.whiteBackground,
          child: ResponsiveRowColumn(
            layout: ResponsiveRowColumnType.COLUMN,
            children: [ResponsiveRowColumnItem(child: _allcolumn(context))],
          ),
        ),
      ),
    );
  }
}

Widget _allcolumn(BuildContext context) {
  return Column(
    children: [
      _wrappe2container(context),
      _wrappe2container2(context),
      _wrappe2container3(context),
      _wrappe2container4(context)
    ],
  );
}

Widget _wrappe2container(BuildContext context) {
  return Container(
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(34),
    color: AppColors.whiteBackground,
    child: _containercolumn(context),
  );
}

Widget _wrappe2container2(BuildContext context) {
  return Container(
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(36),
    color: AppColors.whiteBackground,
    child: _containercolumn1(context),
  );
}

Widget _wrappe2container3(BuildContext context) {
  return Container(
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(10),
    color: AppColors.whiteBackground,
    child: _containercolumn2(context),
  );
}

Widget _wrappe2container4(BuildContext context) {
  return Container(
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(7),
    color: AppColors.whiteBackground,
    child: _containercolumn3(context),
  );
}

Widget _containercolumn(BuildContext context) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.end,
    children: const [ Container1(), Container2()],
  );
}

Widget _containercolumn1(BuildContext context) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children:const [Container3()],
  );
}

Widget _containercolumn2(BuildContext context) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.end,
    children: const [Container4()],
  );
}

Widget _containercolumn3(BuildContext context) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.end,
    children: const [Container5()],
  );
}