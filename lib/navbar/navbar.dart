// import 'package:flutter/material.dart';

// class Navbar1 extends StatelessWidget {
//   const Navbar1({super.key});

//   @override
//   Widget build(BuildContext context) {
//     final List<Widget> children = [
//       Center(
//         child: Container(
//           height:
//               850, // use 'MediaQuery.of(context).size.height' to fit the screen height,
//           color: Colors.red,
//         ),
//       )
//     ];

//     return MaterialApp(
//         debugShowCheckedModeBanner: false,
//         home: Scaffold(
//           body: Stack(
//             children: <Widget>[
//               children[0],
//               Positioned(
//                 left: 0,
//                 right: 0,
//                 bottom: 0,
//                 child: bottomNavigationBar(),
//               ),
//             ],
//           ),
//         ));
//   }
// }

// Widget bottomNavigationBar() {
//   return Container(
//     margin: EdgeInsets.only(left: 16, right: 16),
//     decoration: BoxDecoration(
//       color: Colors.amber,
//       borderRadius: BorderRadius.only(
//           topLeft: Radius.circular(200), topRight: Radius.circular(200)),
//     ),
//     child: BottomNavigationBar(
//       backgroundColor: Colors.transparent,
//       showUnselectedLabels: true,
//       type: BottomNavigationBarType.fixed,
//       elevation: 0,
//       items: [
//         BottomNavigationBarItem(
//           icon: Icon(Icons.home),
//         ),
//         BottomNavigationBarItem(
//           icon: Icon(Icons.local_activity),
//         ),
//         BottomNavigationBarItem(
//           icon: Icon(Icons.inbox),
//         ),
//         BottomNavigationBarItem(
//           icon: Icon(Icons.person),
//         ),
//       ],
//     ),
//   );
// }
