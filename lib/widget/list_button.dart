import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:health/button_change.dart';
import 'package:health/math/math_1.dart';
import 'package:health/utils/app_colors.dart';
import 'package:get/get.dart';

import '../utils/size_config.dart';
import '../controller/controller.dart';

// ignore: must_be_immutable

class ListButton extends StatelessWidget {
  const ListButton({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Padding(
      padding: EdgeInsets.only(bottom: SizeConfig.vertical(1)),
      child: SizedBox(
          height: SizeConfig.vertical(10),
          width: SizeConfig.screenWidth,
          child: _buttonall(context)),
    );
  }
}

// Widget _listbutton(BuildContext context) {
//   return ListView(
//     scrollDirection: Axis.horizontal,
//     children: <Widget>[
//       _buttonall(context),
//     ],
//   );
// }

Widget _buttonall(BuildContext context) {
  return Container(
    margin: const EdgeInsets.all(10),
    width: SizeConfig.horizontal(20),
    child: ListView(
      scrollDirection: Axis.horizontal,
      children: [
        _buttontop(context),
        _buttonmath(context),
        _buttonalgebra(context),
        _buttongeometri(context),
        _buttontrigonometri(context),
      ],
    ),
  );
}

// ElevatedButton.styleFrom(
//             backgroundColor: AppColors.whiteBackground,
//             shadowColor: Colors.transparent,
//             shape: RoundedRectangleBorder(
//               borderRadius: BorderRadius.circular(15)
//             )
//           ),

Widget _buttontop(BuildContext context) {
  ButtonController buttonController = ButtonController();
  return Container(
    margin: const EdgeInsets.all(10),
    width: SizeConfig.horizontal(20),
    child: Obx(
      () {
        return ElevatedButton(
          onPressed: () {
            if (buttonController.enabledButton.value) {
              buttonController.enabledButton.value = false;
            } else {
              buttonController.enabledButton.value = true;
            }
          },
          style: buttonController.enabledButton.value
              ? btnChange().btnchange1(true)
              : btnChange().btnchange1(false),
          child: Text(
            'Top',
            style: TextStyle(color: AppColors.darkBackground),
          ),
        );
      },
    ),
  );
}

Widget _buttonmath(BuildContext context) {
  ButtonController buttonController = ButtonController();
  return Container(
      margin: const EdgeInsets.all(10),
      width: SizeConfig.horizontal(20),
      child: Obx(
      () {
        return ElevatedButton(
          onPressed: () {
            if (buttonController.enabledButton.value) {
              buttonController.enabledButton.value = false;
            } else {
              buttonController.enabledButton.value = true;
            }
            log(buttonController.enabledButton.value.toString());
          },
          style: buttonController.enabledButton.value
              ? btnChange().btnchange1(true)
              : btnChange().btnchange1(false),
          child: Text(
            'Math',
            style: TextStyle(color: AppColors.darkBackground),
          ),
        );
      },
    ),);
}

Widget _buttonalgebra(BuildContext context) {
  return Container(
    margin: const EdgeInsets.all(10),
    width: SizeConfig.horizontal(22),
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
          backgroundColor: AppColors.whiteBackground,
          shadowColor: Colors.transparent,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15))),
      onPressed: () {},
      child: Text(
        'Algebra',
        style: TextStyle(color: AppColors.darkBackground),
      ),
    ),
  );
}

Widget _buttongeometri(BuildContext context) {
  return Container(
    margin: const EdgeInsets.all(10),
    width: SizeConfig.horizontal(25),
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
          backgroundColor: AppColors.whiteBackground,
          shadowColor: Colors.transparent,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15))),
      onPressed: () {},
      child: Text(
        'Geometri',
        style: TextStyle(color: AppColors.darkBackground),
      ),
    ),
  );
}

Widget _buttontrigonometri(BuildContext context) {
  return Container(
    margin: const EdgeInsets.all(10),
    width: SizeConfig.horizontal(30.5),
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
          backgroundColor: AppColors.whiteBackground,
          shadowColor: Colors.transparent,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15))),
      onPressed: () {},
      child: Text(
        'Trigonometri',
        style: TextStyle(color: AppColors.darkBackground),
      ),
    ),
  );
}
