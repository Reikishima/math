import 'package:flutter/material.dart';
import '../utils/app_colors.dart';
import '../utils/size_config.dart';

class RowUp extends StatelessWidget {
  const RowUp({super.key});

  @override
Widget build(BuildContext context) {
  return  Container(
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(5),
      color: AppColors.greenInfo,
      child: _icon(context),
    );
}

Widget _icon(BuildContext context) {
 return Row(
  mainAxisAlignment: MainAxisAlignment.spaceBetween,
  children: [
    IconButton(
        icon: Icon(
          Icons.notifications_none_outlined,
          color: AppColors.darkBackground,
        ),
        onPressed: () {},
      ),
      IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.search_outlined,
            color: AppColors.darkBackground,
          ),)
  ],
 );
}


}