import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'package:health/message/message.dart';
import 'package:health/navbar/navbar.dart';

import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

class Button1 extends StatelessWidget {
  const Button1({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(62.5),
      color: AppColors.whiteBackground,
      child: _allwrapper(context),
    );
  }
}

Widget _allwrapper(BuildContext context) {
  return Column(
    children: [_buttongradient(context), _bottomnavbar(context)],
  );
}

Widget _buttongradient(BuildContext context) {
  return Column(
    children: [_row1(context), _row2(context)],
  );
}

Widget _bottomnavbar(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(top: SizeConfig.vertical(2)),
    child: Container(
      width: SizeConfig.horizontal(70),
      height: SizeConfig.vertical(7),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15), color: Colors.transparent),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.dashboard_customize_rounded,
              color: AppColors.pinkky,
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.bolt_outlined,
              color: AppColors.greyDisabled,
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.message_outlined,
              color: AppColors.greyDisabled,
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.person_outline_outlined,
              color: AppColors.greyDisabled,
            ),
          ),
        ],
      ),
    ),
  );
}


Widget _row1(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(top: SizeConfig.vertical(1)),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [_button1(context), _button2(context)],
    ),
  );
}

Widget _row2(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(
      left: SizeConfig.horizontal(3),
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [_button3(context), _button4(context)],
    ),
  );
}

Widget _button1(BuildContext context) {
  return Container(
    width: SizeConfig.horizontal(40),
    height: SizeConfig.vertical(25),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(15),
      gradient: LinearGradient(
          colors: <Color>[AppColors.cyan, AppColors.whiteSkeleton],
          begin: Alignment.topRight,
          end: Alignment.bottomLeft),
    ),
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          backgroundColor: Colors.transparent),
      onPressed: () {
        // Get.to(Navbar1());
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            margin: EdgeInsets.only(
                bottom: SizeConfig.vertical(5),
                right: SizeConfig.horizontal(10)),
            width: SizeConfig.horizontal(25),
            child: Text(
              'Probability Theory',
              style: TextStyle(
                  fontFamily: 'NunitoRegular',
                  fontSize: 16,
                  fontWeight: FontWeight.w900,
                  color: AppColors.whiteBackground),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: SizeConfig.horizontal(10),
            ),
            width: SizeConfig.horizontal(30),
            height: SizeConfig.vertical(10),
            child: const CircleAvatar(
                maxRadius: 40,
                backgroundImage: AssetImage('assets/images/download.jpg')),
          ),
        ],
      ),
    ),
  );
}

Widget _button2(BuildContext context) {
  return Container(
    width: SizeConfig.horizontal(40),
    height: SizeConfig.vertical(20),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(15),
      gradient: LinearGradient(
          colors: <Color>[AppColors.purple, AppColors.whiteSkeleton],
          begin: Alignment.topRight,
          end: Alignment.bottomLeft),
    ),
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          backgroundColor: Colors.transparent),
      onPressed: () {
        Get.to(const Message());
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            margin: EdgeInsets.only(right: SizeConfig.horizontal(10)),
            width: SizeConfig.horizontal(25),
            child: Text(
              'Internation Subjects',
              style: TextStyle(
                  fontFamily: 'NunitoRegular',
                  fontSize: 16,
                  fontWeight: FontWeight.w900,
                  color: AppColors.whiteBackground),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: SizeConfig.horizontal(10),
            ),
            width: SizeConfig.horizontal(15),
            height: SizeConfig.vertical(10),
            child: const CircleAvatar(
                maxRadius: 5,
                backgroundImage: AssetImage('assets/images/download.jpg')),
          ),
        ],
      ),
    ),
  );
}

Widget _button3(BuildContext context) {
  return Container(
    width: SizeConfig.horizontal(40),
    height: SizeConfig.vertical(20),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(15),
      gradient: LinearGradient(
          colors: <Color>[AppColors.pinkky, AppColors.whiteSkeleton],
          begin: Alignment.topRight,
          end: Alignment.bottomLeft),
    ),
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          backgroundColor: Colors.transparent),
      onPressed: () {},
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            margin: EdgeInsets.only(right: SizeConfig.horizontal(10)),
            width: SizeConfig.horizontal(25),
            child: Text(
              'Translation',
              style: TextStyle(
                  fontFamily: 'NunitoRegular',
                  fontSize: 16,
                  fontWeight: FontWeight.w900,
                  color: AppColors.whiteBackground),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: SizeConfig.horizontal(10),
            ),
            width: SizeConfig.horizontal(15),
            height: SizeConfig.vertical(10),
            child: const CircleAvatar(
                maxRadius: 5,
                backgroundImage: AssetImage('assets/images/download.jpg')),
          ),
        ],
      ),
    ),
  );
}

Widget _button4(BuildContext context) {
  return Container(
    width: SizeConfig.horizontal(40),
    height: SizeConfig.vertical(25),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(15),
      gradient: LinearGradient(
          colors: <Color>[AppColors.cyan, AppColors.whiteSkeleton],
          begin: Alignment.topCenter,
          end: Alignment.bottomLeft),
    ),
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          backgroundColor: Colors.transparent),
      onPressed: () {},
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            margin: EdgeInsets.only(
                bottom: SizeConfig.vertical(5),
                right: SizeConfig.horizontal(10)),
            width: SizeConfig.horizontal(25),
            child: Text(
              'Physics',
              style: TextStyle(
                  fontFamily: 'NunitoRegular',
                  fontSize: 16,
                  fontWeight: FontWeight.w900,
                  color: AppColors.whiteBackground),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: SizeConfig.horizontal(10),
            ),
            width: SizeConfig.horizontal(30),
            height: SizeConfig.vertical(10),
            child: const CircleAvatar(
                maxRadius: 40,
                backgroundImage: AssetImage('assets/images/download.jpg')),
          ),
        ],
      ),
    ),
  );
}
