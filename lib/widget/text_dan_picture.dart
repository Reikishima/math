import 'package:flutter/material.dart';
import 'package:health/utils/app_colors.dart';

import '../utils/size_config.dart';

class TextandPicture extends StatelessWidget {
  const TextandPicture({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SizedBox(
        width: SizeConfig.screenWidth,
        height: SizeConfig.vertical(15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [_text(context), _circleimage(context)],
        ));
  }
}

Widget _text(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(
      left: SizeConfig.horizontal(5),
    ),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        RichText(
          text: TextSpan(
              text: 'Hi Kristi!\n',
              style: TextStyle(
                  color: AppColors.darkBackground,
                  fontSize: 30,
                  fontFamily: 'NunitoRegular',
                  fontWeight: FontWeight.w700),
              children: <TextSpan>[
                TextSpan(
                    text: 'We Found Out Some\n',
                    style: TextStyle(
                        fontFamily: 'NunitoRegular',
                        color: AppColors.darkBackground,
                        fontSize: 14,
                        fontWeight: FontWeight.w600)),
                TextSpan(
                    text: 'Interesting Topics For You',
                    style: TextStyle(
                        fontFamily: 'NunitoRegular',
                        color: AppColors.darkBackground,
                        fontSize: 14,
                        fontWeight: FontWeight.w600))
              ]),
        ),
      ],
    ),
  );
}

Widget _circleimage(BuildContext context) {
  return Padding(
    padding: EdgeInsets.only(right: SizeConfig.horizontal(5)),
    child: const CircleAvatar(
      backgroundImage: AssetImage('assets/images/download.jpg'),
      maxRadius: 50,
    ),
  );
}
