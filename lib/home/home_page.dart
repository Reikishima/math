import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/state_manager.dart';
import 'package:health/controller/controller.dart';

import 'package:health/utils/app_colors.dart';
import 'package:health/utils/size_config.dart';

import 'package:health/widget/button.dart';
import 'package:health/widget/list_button.dart';

import 'package:health/widget/text_dan_picture.dart';
import 'package:responsive_framework/responsive_framework.dart';

import '../widget/tabbar.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: AppColors.greyDisabled,
        ),
        backgroundColor: AppColors.whiteBackground,
        shadowColor: Colors.transparent,
        leading: Icon(
          Icons.notifications_none_outlined,
          color: AppColors.darkBackground,
        ),
        actions: [
          Icon(
            Icons.search_outlined,
            color: AppColors.darkBackground,
          )
        ],
      ),
      body: GetBuilder<ButtonController>(
          init: ButtonController(),
          builder: (ButtonController controller) {
            return SafeArea(
              child: DefaultTabController(
                length: 5,
                child: Column(
                  children: <Widget>[
                    Container(
                      color: AppColors.whiteBackground,
                      width: SizeConfig.screenWidth,
                      height: SizeConfig.vertical(88.7),
                      child: ResponsiveRowColumn(
                        layout: ResponsiveRowColumnType.COLUMN,
                        children: <ResponsiveRowColumnItem>[
                          ResponsiveRowColumnItem(child: _warpper(context))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }
}

Widget _warpper(BuildContext context) {
  return SizedBox(
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(88.7),
    child: Column(
      children: const [
        TextandPicture(),
        Tabbar(),
        Button1(),
      ],
    ),
  );
}
