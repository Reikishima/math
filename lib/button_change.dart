import 'package:flutter/material.dart';

import 'utils/app_colors.dart';

class btnChange {
  ButtonStyle btnchange1(bool changeColor) {
    return ButtonStyle(
      backgroundColor: changeColor
          ? MaterialStateProperty.all<Color>(AppColors.pinkky)
          : MaterialStateProperty.all<Color>(AppColors.whiteBackground),
      foregroundColor: changeColor
          ? MaterialStateProperty.all<Color>(AppColors.whiteBackground)
          : MaterialStateProperty.all<Color>(AppColors.pinkky),
      shadowColor: MaterialStateProperty.all<Color>(Colors.transparent),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }
}
